<?php

$root = "http://".$_SERVER['HTTP_HOST'];
$root .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
$_CONFIG['app']['url']    = $root;

DEFINE('BASE_URL',$_CONFIG['app']['url']);
DEFINE('SITE_URL',$_CONFIG['app']['url']);

$_CONFIG['app']['tpl']='tpl';

//true : display with templates. false : display content from mods.
$_CONFIG['app']['disp']=true;

$_CONFIG['app']['menus']=array();

$_CONFIG['app']['pagination']=10;

$_CONFIG['app']['groups']=array('admin','operator');


$_CONFIG['mod']['base']='home';
$_CONFIG['mod']['act']='index';

$title='Selamat Datang';
$content=$title.' di aplikasi ini';
