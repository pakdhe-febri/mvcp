<?php if ( ! defined('BASE_URL')) exit('No direct script access allowed');
class Template {
  var $vars; /// Holds all the template variables

    /**
     * Constructor
     *
     * @param $file string the file name you want to load
     */
    function Template($file = null) {
        $this->file = $file;
    }

    /**
     * Set a template variable.
     */
    function set($name, $value) {
        $this->vars[$name] = is_object($value) ? $value->fetch() : $value;
    }

    /**
     * Open, parse, and return the template file.
     *
     * @param $file string the template file name
     */
    function fetch($file = null) {
        if(!$file) $file = $this->file;

        if($this->vars) extract($this->vars);          // Extract the vars to local namespace
        ob_start();                    // Start output buffering
        include($file);                // Include the file
        $contents = ob_get_contents(); // Get the contents of the buffer
        ob_end_clean();                // End buffering and discard
        return $contents;              // Return the contents
    }
	
		function js_url($path='',$modules='') {
			$path_url=(!empty($modules)) ? 'assets/modules/'.$modules.'/js/'.$path : 'assets/js/'.$path;
			return '<script type="text/javascript" src="'.BASE_URL.$path_url.'"></script>';
		}
	
		function css_url($path='',$modules='') {
			$path_url=(!empty($modules)) ? 'assets/modules/'.$modules.'/css/'.$path : 'assets/css/'.$path;
			return '<link rel="stylesheet" type="text/css" href="'.BASE_URL.$path_url.'" />';
		}
}