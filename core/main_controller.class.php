<?php 
require_once "template.class.php";

class main_controller {
	// GLOBAL OBJECT 
	var $instance;
	public $modules;
	// DEFAULT TEMPLATE N SKELETON :
	var $menu;
	var $title;
	var $body;
	// DEFAULT CONFIGURASI FROM config.php
	var $cfg;

	// DEFAULT mysql
	var $db;

	function main_controller($mod=''){
		global $_CONFIG;
		$this->cfg=$_CONFIG;
		$this->db= new MySQL();
		$this->instance->body['js']='';
		$this->instance->body['content']='';
		
		$sql='select * from m_config;';
		$res = $this->db->QueryArray($sql,true);
		foreach($res as $k=>$v){
			$this->cfg[$v['name']] =$v['value']; 
		}
		
	}

// LOADER FUNCTION :	
// all url must had : index.php?mod=module_name&act=method_of_module_name
// default value : mod = welcome (on config.php files) and act=index
	function set_module($mod=''){
		// MAIN CONTROLLER LOADER HERE with modules !!
		if(file_exists('./controllers/'.$mod.'.php')) {
			require_once './controllers/'.$mod.'.php';
			$this->instance = new $mod;
		}else {
			die('file <strong>'.$mod.'.php</strong> not exist on directory <strong>controller</strong>');
		}
	}

	function set_action($act=''){
		if(empty($act)){
			$act = 'index';
		}
		if(method_exists($this->instance,$act)){
			$this->instance->$act();
		}else{
			$this->instance->index();
		}
	}
// END OF LOADER FUNCTION

// HELPER CONTROLLER FUNC
	function helper($file_helper=''){
		if(file_exists('./helpers/'.$file_helper)) include_once('./helpers/'.$file_helper);
	}
	
	function set_js(){
		if(empty($this->instance->body['js'])) {
			$js = '';
		}else{
			$js = $this->instance->body['js'];
		}
		return $js;
	}
	
// END TEMPLATE ENGINE CONTROLLER FUNC
	
// TEMPLATE ENGINE CONTROLLER DISPLAY
	function view($tpl_file='',$data='',$is_return=FALSE){
		if(file_exists('./views/'.$tpl_file)){
			$mytpl = new template('./views/'.$tpl_file);
		}else{
			die('no view file <strong>'.$tpl_file.'</strong> found');
		}
		$mytpl->set('_CONFIG',$this->cfg);

		if(is_array($data)){
			foreach($data as $k=>$v){
				$mytpl->set($k,$v);
			}
		}

		if($is_return){
			//print_r($mytpl->fetch());
			return $mytpl->fetch();
		}else{
			echo $mytpl->fetch();
		}
		
	}

	function display($index='index.php'){
		if(isset($this->instance->body)){
			$tpl =  new template('./views/FRONTEND/'.$index);
			$tpl->set('_CONFIG',$this->cfg);
			
			$myjs = $this->set_js();
			$tpl->set('js',$myjs);
			foreach($this->instance->body as $k=>$v)
				$tpl->set($k,$v);
			
			echo $tpl->fetch();
		}else{
			die('no instance body');
		}
	}
	
// END TEMPLATE ENGINE CONTROLLER DISPLAY 
	function redirect($path){
		if(!empty($path))
			header('Location:'.BASE_URL.$path);
	}
	
	function check_auth(){
		$res=false;
		//dari post login
		if((isset($_POST['user_name']))&&(isset($_POST['user_passwd']))){
			$sql = 'select * from d_users where uname="'.$_POST['user_name'].'" and upasswd="'.md5($_POST['user_passwd']).'";';
			$res_db = $this->db->QueryArray($sql,true);
			if((isset($res_db[0]))&&(count($res_db))){
				$_SESSION=$res_db[0];
				$_SESSION['discard_after'] = time() + 3600;
				$res=true;
			}else{
				$res=false;
			}
		}else{
			//check session
			//$res=false;
		}
		return $res;
	}

}

class admin_controller extends main_controller{
	var $is_auth=true;

	function __construct(){
		parent::__construct();
	}
	
	function set_session($sess_users=array()){
		$now = time();
		if ((isset($_SESSION['discard_after'])) && ($now >= $_SESSION['discard_after'])) {
			die('redirect??');
				// this session has worn out its welcome; kill it and start a brand new one
				session_unset();
				session_destroy();
				$this->redirect('index.php?mod=home&act=logout');
		}elseif(isset($sess_users['id'])){
			if(!isset($_SESSION)) session_start();
			$_SESSION = $sess_users;
			// either new or old, it should live at most for another hour
			$_SESSION['discard_after'] = $now + 3600;
		}else{
			$this->redirect('index.php?mod=home&act=logout');
		}
	}	
	
	function display($index='index.php'){
		if(isset($this->instance->body)){
			$this->set_session($_SESSION);
			$tpl =  new template('./views/BACKEND/'.$index);
			$tpl->set('_CONFIG',$this->cfg);
			
			$myjs = $this->set_js();
			$tpl->set('js',$myjs);
			foreach($this->instance->body as $k=>$v)
				$tpl->set($k,$v);
			
			echo $tpl->fetch();
		}else{
			die('no instance body');
		}
	}
	
	
}