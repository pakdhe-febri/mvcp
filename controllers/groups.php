<?php if (!defined('BASE_URL')) exit('No direct script access allowed');

class Groups extends admin_controller {
	
	function __construct(){
		parent::__construct();
		$this->set_session($_SESSION);
		$this->mods = 'groups';
		$this->tbl = 'm_groups';
	}
	
	function index(){
		$this->instance->body['title']='<i class="glyphicon glyphicon-th-large"></i>&nbsp;Groups';
		$this->instance->body['description']='Manage Groups';
		$data['ls_user']=array();
		$sql = 'select * from '.$this->tbl.';';
		$data['ls_user'] = $this->db->QueryArray($sql,true);
		$data['mods']=$this->mods;
		$data['tbl']=$this->tbl;
		$html=$this->view('ls_groups.php',$data,true);
		$this->instance->body['content']=$html;
		$this->display();		
	}
	
	function add(){
		$resp = array(
			'resp'=>0,
			'msg'=>'Error'
		);
		$resp['msg']=$_POST;
		
		$values['name'] = MySQL::SQLValue($_POST['name']);
		
		$result = $this->db->InsertRow($this->tbl, $values);
		if($result){
			$resp = array(
				'resp'=>1,
				'msg'=>'Berhasil Menambah '.ucwords($this->mods)
			);
		}else{
			$resp = array(
				'resp'=>0,
				'msg'=>'Gagal Menambah '.ucwords($this->mods)
			);		
		}
		
		header('Content-Type: application/json');
		echo json_encode($resp);
	}
	
	function edit(){
		$resp = array(
			'resp'=>0,
			'msg'=>'Error'
		);
		$resp['msg']=$_POST;
		$values['name'] = MySQL::SQLValue($_POST['name']);
		
		$filter['id'] = MySQL::SQLValue($_POST['id']);
		
		$result = $this->db->UpdateRows($this->tbl, $values,$filter);
		if($result){
			$resp = array(
				'resp'=>1,
				'msg'=>'Berhasil Mengubah '.ucwords($this->mods)
			);
		}else{
			$resp = array(
				'resp'=>0,
				'msg'=>'Gagal Mengubah '.ucwords($this->mods)
			);		
		}
		
		header('Content-Type: application/json');
		echo json_encode($resp);		
	}
	
	function del(){
		if(isset($_GET['id'])){
			$filter['id']=$_GET['id'];
			$result = $this->db->DeleteRows($this->tbl, $filter);
		}
		$this->redirect('index.php?mod='.$this->mods);		
	}
	
}
	