<?php if (!defined('BASE_URL')) exit('No direct script access allowed');

class users extends admin_controller {
	
	function __construct(){
		parent::__construct();
		$this->set_session($_SESSION);
		$this->mods = 'users';
		$this->tbl = 'd_users';
	}
	
	function index(){
		$this->instance->body['title']='<i class="glyphicon glyphicon-user"></i>&nbsp;'.ucwords($this->mods);
		$this->instance->body['description']='Manage '.ucwords($this->mods);
		$data['ls_user']=array();
		$sql = 'select * from d_users;';
		$data['ls_user'] = $this->db->QueryArray($sql,true);
		$data['mods']=$this->mods;
		$data['tbl']=$this->tbl;
		$html=$this->view('ls_users.php',$data,true);
		$this->instance->body['content']=$html;
		$this->display();		
	}
	
	function add(){
		$resp = array(
			'resp'=>0,
			'msg'=>'Error'
		);
		$resp['msg']=$_POST;
		$values['uname'] = MySQL::SQLValue($_POST['uname']);
		$values['upasswd'] = MySQL::SQLValue(md5($_POST['upasswd']));
		$values['full_name'] = MySQL::SQLValue($_POST['full_name']);
		$values['nip'] = MySQL::SQLValue($_POST['nip']);
		$values['mail'] = MySQL::SQLValue($_POST['mail']);
		$values['phone'] = MySQL::SQLValue($_POST['phone']);
		
		$result = $this->db->InsertRow($this->tbl, $values);
		if($result){
			$resp = array(
				'resp'=>1,
				'msg'=>'Berhasil Menambah Users'
			);
		}else{
			$resp = array(
				'resp'=>0,
				'msg'=>'Gagal Menambah Users'
			);		
		}
		
		header('Content-Type: application/json');
		echo json_encode($resp);
	}
	
	function edit(){
		$resp = array(
			'resp'=>0,
			'msg'=>'Error'
		);
		$resp['msg']=$_POST;
		$values['uname'] = MySQL::SQLValue($_POST['uname']);
		if(!empty($_POST['upasswd']))
			$values['upasswd'] = MySQL::SQLValue(md5($_POST['upasswd']));
		
		$values['full_name'] = MySQL::SQLValue($_POST['full_name']);
		$values['nip'] = MySQL::SQLValue($_POST['nip']);
		$values['mail'] = MySQL::SQLValue($_POST['mail']);
		$values['phone'] = MySQL::SQLValue($_POST['phone']);
		
		$filter['id'] = MySQL::SQLValue($_POST['id']);
		
		$result = $this->db->UpdateRows($this->tbl, $values,$filter);
		if($result){
			$resp = array(
				'resp'=>1,
				'msg'=>'Berhasil Mengubah Users'
			);
		}else{
			$resp = array(
				'resp'=>0,
				'msg'=>'Gagal Mengubah Users'
			);		
		}
		
		header('Content-Type: application/json');
		echo json_encode($resp);		
	}
	
	function del(){
		if(isset($_GET['id'])){
			$filter['id']=$_GET['id'];
			$result = $this->db->DeleteRows($this->tbl, $filter);
		}
		$this->redirect('index.php?mod='.$this->mods);		
	}
	
}
	