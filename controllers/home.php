<?php if (!defined('BASE_URL')) exit('No direct script access allowed');

class home extends main_controller {
	
	function __construct(){
		parent::__construct();
		$this->instance->body['js']='';
	}
	
	function index(){
		if(isset($_SESSION['id'])) $this->redirect('index.php?mod=dashboard');		
		$mydata=array();
		$html = $this->view('FRONTEND/form_login.php',$mydata,true);
		$this->instance->body['content']=$html;
		$this->display();
	}
	
	function login(){
		$res=array(
			'result'=>false,
			'msg'=>'',
			'data'=>array()
		);
		$check = $this->check_auth();
		if($check){
			$res['result']=true;
			$res['data']['redirect']=BASE_URL.'index.php?mod=dashboard';
		}else{
			$res['result']=false;
			$res['msg']="Username & Password ada yang salah.\nSilahkan ulangi sekali lagi.";
		}
		echo json_encode($res);
	}
	
	function logout(){
		//unset all session
		session_unset();
		session_destroy();
		$this->redirect('index.php?mod=home');
		exit;		
	}
	
}