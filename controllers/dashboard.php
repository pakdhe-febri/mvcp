<?php if (!defined('BASE_URL')) exit('No direct script access allowed');

class dashboard extends admin_controller {
	
	function __construct(){
		parent::__construct();
		$this->set_session($_SESSION);
	}
	
	function index(){
		$html=$this->cfg['welcome_msg'];
		$this->instance->body['title']='<i class="glyphicon glyphicon-cog"></i>&nbsp;Dashboard';
		$this->instance->body['description']='Dashboard App';
		$this->instance->body['content']=$html;
		$this->display();		
	}

}
	