<p>
	<button type="button" class="btn btn-sm btn-primary" id="btn-add" name="btn-add">
		<i class="glyphicon glyphicon-plus-sign"></i>&nbsp;
		Add
	</button>
</p>
<p>
	<table class="table table-responsive table-bordered table-condensed table-striped">
		<thead>
			<tr>
				<th class="text-center">No.</th>
				<th class="text-center">User Name</th>
				<th class="text-center">Full Name</th>
				<th class="text-center">NIP</th>
				<th class="text-center">Mail</th>
				<th class="text-center">Phone</th>
				<th class="text-center">Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=1;foreach($ls_user as $k=>$v):?>
			
			<tr>
				<td class="text-right" valign="middle"><?php echo $i++;?>.</td>
				<td><?php echo $v['uname'];?></td>
				<td><?php echo $v['full_name'];?></td>
				<td><?php echo $v['nip'];?></td>
				<td><?php echo $v['mail'];?></td>
				<td><?php echo $v['phone'];?></td>
				<td class="text-center">
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-sm btn-info btn-edit" data-obj='<?php echo json_encode($v);?>' id="btn-edit" name="btn-edit">
							<i class="glyphicon glyphicon-ok-sign"></i>&nbsp;
							Edit
						</button>
						<button class="btn btn-sm btn-danger btn-del" data-id="<?php echo $v['id'];?>" id="btn-del" name="btn-del">
							<i class="glyphicon glyphicon-remove-sign"></i>&nbsp;
							Del
						</button>
					</div>
				</td>
			</tr>
			<?php endforeach;?>
		</tbody>	
	</table>
</p>

<div class="modal fade" id="mdl-form" tabindex="-1" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="glyphicon glyphicon-user"></i>&nbsp;Form User</h4>
			</div>
      <form id="frm" name="frm" >
				<input type="hidden" name="id" id="id" value=""/>
				<input type="hidden" name="act" id="act" value="add" />
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Username</label>
								<input type="text" class="form-control" id="uname" name="uname" placeholder="User Name" />
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" id="upasswd" name="upasswd" />
							</div>
							<div class="form-group">
								<label>Full Name</label>
								<input type="text" class="form-control" id="full_name" name="full_name" placeholder="Full Name" />
							</div>	
							
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>NIP</label>
								<input type="text" class="form-control" id="nip" name="nip" placeholder="NIP" />
							</div>	
							<div class="form-group">
								<label>Email Address</label>
								<input type="email" class="form-control" id="mail" name="mail" placeholder="Email">
							</div>
							<div class="form-group">
								<label>Phone</label>
								<input type="text" class="form-control" id="phone" name="phone" placeholder="Telp.">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left bg-black" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('#btn-add').click(function(e){
				$('#frm').trigger('reset');
				$('#act').val('add');
				$('.modal-header').removeClass('modal-header bg-info').addClass('modal-header bg-primary');
				$('#mdl-form').modal('show');
		});
		
		$( ".btn-edit" ).each(function(index) {
			$(this).click(function(e){
				$('#frm').trigger('reset');
				$('#act').val('edit');
				$('.modal-header').removeClass('modal-header bg-primary').addClass('modal-header bg-info');
				$('#mdl-form').modal('show');
				var data_json = $(this).data('obj');
				$.each(data_json,function(k,v){
					if(k!=='upasswd')
						$('#'+k).val(v);			
				});
			});
		});
		
		$( ".btn-del" ).each(function(index) {
			$(this).click(function(e){
				var id = $(this).data('id');
				var r = confirm("Are you sure to delete this data!");
				if(r == true){
					var url = SITE_URL+'index.php?mod=<?php echo $mods?>&act=del&id='+id;
					window.location.href = url;
				}
					
				e.preventDefault();
				});
		});
		
		$('#frm').submit(function(e){
			var act = $('#act').val();
			var url = SITE_URL+'index.php?mod=<?php echo $mods?>&act='+act;
			var data = $(this).serialize();
			$.ajax({
				method: "POST",
				url: url,
				data:data,
				dataType: "json",
				success: function(res){
					alert(res.msg);
					$('#mdl-form').modal('hide');
					var url = SITE_URL+'index.php?mod=<?php echo $mods?>';
					window.location.href = url;
				}
			});

			console.log(url);
			e.preventDefault();
		});
		
	});
</script>