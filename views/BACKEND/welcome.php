<div class="row-fluid">
	<div class="col-md-4" style="padding-left:0px;">
		<div id="panel-user" style="padding:0px;">
			<table id="pg-user"></table>
			<div id="toolbar" style="padding: 5px;width:360px;">
					<a href="javascript:void(0)" class="easyui-linkbutton pull-right" iconCls="icon-save" onclick="saveProfile();">Save</a>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div id="panel-dashboard-cfg">
			<table class="table table-bordered table-striped table-responsive table-condensed" style="margin-bottom:0px;">
			<?php foreach($m_config['Office'] as $k=>$v):?>
				<tr>
				<td><i class="glyphicon glyphicon-cog"></i>&nbsp;<?php echo ucwords(str_replace('_',' ',$k));?></td>
				<td><span id="<?php echo $k;?>"><?php echo $v;?></span></td>
				</tr>
			<?php endforeach;?>
			</table>
		</div>
	</div>
	<div class="col-md-4">
		<?php if($_SESSION['thn_anggaran']): ?>
		<div id="panel-simple-stat">
			<table class="table table-bordered table-striped table-responsive table-condensed" style="margin-bottom:0px;">
				<tbody>
				<tr>
					<td><i class="glyphicon glyphicon-calendar"></i>&nbsp;THN ANGGARAN</td>
					<td><span class="pull-right"><?php echo $_SESSION['thn_anggaran'];?></span></td>
				</tr>
				<tr>
					<td><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;PENDAPATAN</td>
					<td><span class="pull-right" id="dashboard_pendapatan">Rp. 123.109.109.-</span></td>
				</tr>
				<tr>
					<td><i class="glyphicon glyphicon-minus-sign"></i>&nbsp;BELANJA</td>
					<td><span class="pull-right" id="dashboard_belanja"></span></td>
				</tr>
				<tr>
					<td><i class="glyphicon glyphicon-minus-sign"></i>&nbsp;PEMBIAYAAN</td>
					<td><span class="pull-right" id="dashboard_pembiayaan"></span></td>
				</tr>
				<tr>
					<td><i class="glyphicon glyphicon-ok-sign"></i>&nbsp;TOTAL</td>
					<td><span class="pull-right" id="dashboard_total"></span></td>
				</tr>
				<tr>
					<td><i class="glyphicon glyphicon-exclamation-sign"></i>
						&nbsp;<a href="#" data-toggle="tooltip" title="<center>Maximum<br />Bidang Penyelenggaraan Pemerintahan Gampong<br/>(30% Pendapatan)</center>">Max 30%</a>
						</td>
					<td><span class="pull-right" id="dashboard_max_30"></span></td>
				</tr>				
				</tbody>
			</table>
		</div>
		<?php endif;?>
	</div>	
</div>

<script>
	function saveProfile(){
		var mydata=$('#pg-user').propertygrid('getData');
		var postData={};
		$.each(mydata.rows, function(idx,val){
			postData[val.field]=val.value;
		});
		$.ajax({
			type: "POST",
			url: SITE_URL+"index.php?mod=dashboard&act=saveProfile",
			dataType: "json",
			data: postData,
			success: function(data){
				if(data.success){					
					$.messager.alert('Success',data.msg);
					$('#pg-user').propertygrid('reload');
				}else{
					$.messager.alert('Warning',data.msg);
				}
			}
		});
	};
	<?php if($_SESSION['thn_anggaran']):?>
	function getResume(){
		$.ajax({
			url: BASE_URL+"index.php?mod=apbg&act=get_resume&json=true", 
			dataType:'json',
			success: function(data){
				$('#dashboard_pendapatan').html(toRp(data.pendapatan));
				$('#dashboard_belanja').html(toRp(data.belanja));
				$('#dashboard_pembiayaan').html(toRp(data.pembiayaan));
				$('#dashboard_total').html(toRp(data.total));
				$('#dashboard_max_30').html(toRp(data.max_30));
			}
		});
	};
	getResume();
	<?php endif;?>
	
	function get_mconfig(){
		$.ajax({
			url: BASE_URL+"index.php?mod=dashboard&act=get_mconfig", 
			dataType:'json',
			success: function(data){
			<?php foreach($m_config['Office'] as $k=>$v):?>
				$('#<?php echo $k;?>').html(data.<?php echo $k;?>);
			<?php endforeach;?>
			}
		});		
	}
	
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip({
			position:'top',
			onShow: function(){
				$(this).tooltip('tip').css({
					backgroundColor: '#666',
					borderColor: '#666',
					color:'#FFF'
				});
			}
		});
		
		$('#panel-user').panel({
			width:'360px',
			iconCls:'mycls-user-info',
			border:true,
			title:'My Profile',
			footer:'#toolbar',
			tools:[
				{
					iconCls:'icon-reload',
					text :'Reload Data',
					handler:function(){$('#pg-user').propertygrid('reload');}
				}
			]			
		});
		
		$('#pg-user').propertygrid({
				url: BASE_URL+'index.php?mod=dashboard&act=user',
				showGroup: false,
				border: false,
				fitColumns:true,
				scrollbarSize: 0
		});
		
		$('#panel-dashboard-cfg').panel({
			width:'90%',
			iconCls:'mycls-compile',
			border:true,
			title:'Data Konfigurasi',
			tools:[
				{
					iconCls:'icon-reload',
					text :'Reload Data',
					handler:function(){get_mconfig();}
				}]
		});
		
		$('#panel-simple-stat').panel({
			width:'90%',
			iconCls:'mycls-news-info',
			border:true,
			title:'Kondisi Keuangan',
			tools:[
				{
					iconCls:'icon-reload',
					text :'Reload Data',
					handler:function(){getResume();}
				}]
		});
		
	});
</script>