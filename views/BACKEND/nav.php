<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo $_CONFIG['app']['url'];?>index.php?mod=dashboard">
				<i class="<?php echo $_CONFIG['site_icon'];?>"></i>&nbsp;<?php echo $_CONFIG['site_name'];?>
			</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-cog"></i>&nbsp;ACL <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li>
							<a href="<?php echo BASE_URL;?>index.php?mod=users">
								<i class="glyphicon glyphicon-user"></i>&nbsp;User
							</a>
						</li>
            <li>
							<a href="<?php echo BASE_URL;?>index.php?mod=groups">
								<i class="glyphicon glyphicon-th-large"></i>&nbsp;Groups
							</a>
						</li>
          </ul>
        </li>
      </ul>			
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="<?php echo BASE_URL;?>index.php?mod=home&act=logout">
						<i class="glyphicon glyphicon-off"></i>&nbsp;Logout
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>