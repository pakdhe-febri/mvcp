<?php include_once('header.php');?>
<?php include_once('nav.php');?>
<div class="jumbotron">
	<div class="container">
		<h4><?php echo @$title;?></h4>
		<p><?php echo @$description;?></p>
	</div>
</div>	
<div class="container">
	<?php echo $content; ?>
</div>
<?php include_once('footer.php');?>