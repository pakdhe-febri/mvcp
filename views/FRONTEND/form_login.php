<style>
	.login-title{
		color:#28166F;
		text-shadow:0 0 10px #FFFFFF, 0 0 15px #FFFFFF, 0 0 20px #FFFFFF, 0 0 25px #25C7F2, 0 0 40px #25C7F2, 0 0 45px #25C7F2, 0 0 50px #25C7F2, 0 0 70px #25C7F2;		
	}	
</style>

<div class="row" style="padding-top:100px;">
	<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-primary">
			<div class="panel-heading text-center">
				<h2 class="login-title">
					<i class="<?php echo $_CONFIG['site_icon'];?>"></i>&nbsp;
					<?php echo $_CONFIG['site_name'];?>
				</h2>
			</div>
			<div class="panel-footer text-center" style="background-color:#000;color:#FFF;">
				<h5>
					<?php echo $_CONFIG['site_longname'];?>
				</h5>			
			</div>
			<form id="form-login" name="form-login" style="padding:10px;">
				<div class="panel-body" style="border:0px;">
					<div class="form-group">
						<label for="exampleInputEmail1">Username</label>
						<input type="text" class="form-control" name="user_name" id="user_name">
					</div>
					<div class="form-group">
						<label for="exampleInputEmail1">Password</label>
						<input type="password" class="form-control" name="user_passwd" id="user_passwd">
					</div>
				</div>
				<div class="panel-footer clearfix">
					<button class="btn btn-primary pull-right" type="submit"><i class="glyphicon glyphicon-off"></i>&nbsp;Login</button>
				</div>
			</form>
		</div>
		<span class="pull-right">
			<strong style="color:#FFF;">
			<em>&copy;2016</em>
			</strong>		
		</span>		
	</div>		
</div>

<script>
	$(document).ready(function(){
		$('#form-login').submit(function(e){
			var formData = $(this).serialize();
			
			$.ajax({
					type: "POST",
					url: SITE_URL+"index.php?mod=home&act=login",
					dataType: "json",
					data: formData,
					success: function(data){						
						if(data.result){
							window.location.replace(data.data.redirect);
						}else{
							alert("Ada kesalahan.\n\r"+data.msg);
						}
						
					}
			});
			
			e.preventDefault();
		});
	})
</script>