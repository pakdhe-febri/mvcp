<!DOCTYPE html>
<html lang="id">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="<?php echo $_CONFIG['site_author'];?>">
    <meta name="description" content="<?php echo $_CONFIG['site_name'].'-'.$_CONFIG['site_longname'];?>">
    <link rel="shortcut icon" href="<?php echo BASE_URL;?>glyphicons-586-equalizer.png"  type="image/x-icon">
    <title><?php echo $_CONFIG['site_name'].' - '.$_CONFIG['site_longname'];?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_URL;?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">		

		<script src="<?php echo BASE_URL;?>assets/js/jquery.min.js"></script>
		<script src="<?php echo BASE_URL;?>assets/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo BASE_URL;?>assets/js/myloader.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<style>
			html {
				position: relative;
				min-height: 100%;
			}
			body {
				padding-top: 40px;
				padding-bottom: 40px;
				background-attachment: scroll;
				background-color: #888;
				background-image: url("<?php echo BASE_URL;?>assets/images/pattern.jpg");
				background-position: left top;
				background-repeat: repeat;
			}
	</style>
		
	<script>
		var BASE_URL = "<?php echo BASE_URL;?>";
		var SITE_URL = "<?php echo BASE_URL;?>";
	</script>
	<?php echo (isset($js)) ? $js : ''; ?>
  </head>

  <BODY>