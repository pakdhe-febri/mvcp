<?php 
	session_start();
	require_once "core/config.php";	
	require_once "core/mysql.class.php";
	require_once "core/main_controller.class.php";
	
	
	$mc = new main_controller();
	$mod=(isset($_GET['mod'])) ? str_replace('/','',$_GET['mod']) : $_CONFIG['mod']['base'];
	
	$mc->set_module($mod);
	$act=(isset($_GET['act'])) ? $_GET['act'] : '';
	
	$mc->set_action($act);
